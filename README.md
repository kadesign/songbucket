![Songbucket Logo](packages/player/static/sb-logo.svg)

Private audio sharing service with 0% unnecessary features

[![Version](https://img.shields.io/badge/version-pre--alpha-darkgray)]()
[![License](https://img.shields.io/badge/license-MIT-brightgreen)](LICENSE)
[![Maintenance status](https://img.shields.io/maintenance/yes/2021)]()
[![Pipeline status](https://gitlab.com/kadesign/songbucket/badges/master/pipeline.svg)](https://gitlab.com/kadesign/songbucket/-/commits/master)
## Prerequisites
* Node.js >=14.17.0
* Yarn (for local development)
* Docker

## How to start

### Docker

1. Create a database in [Supabase](https://app.supabase.io/) and get database URL and private key
2. Create `.env` file in the root directory or configure required environment variables (see [template file `.env.tmpl`](./.env.tmpl))
3. Start cluster: 

```
docker-compose up
```

### Run locally (development mode)

1. Create a database in [Supabase](https://app.supabase.io/) and get database URL and private key
2. *(Optional)* Install and start [Redis](https://redis.io/)
3. Create `.env` files in the root directories of each service in `packages` directory (see respective template files `.env.tmpl` for details)
4. Install dependencies:

```
yarn
```

5. Start services:

```
yarn dev
```
