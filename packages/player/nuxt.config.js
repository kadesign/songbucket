export default {
  globals: {
    id: '__songbucket',
    context: '__SONGBUCKET__'
  },
  head: {
    title: 'Songbucket',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, minimum-scale=1.0' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com', crossorigin: true },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Mukta:wght@300;400;500;600;700&display=swap' }
    ]
  },

  css: [
    '~/assets/styles/global.scss',
    '~/assets/styles/color-schemes/_standard.scss'
  ],

  plugins: [
    { src: '~/plugins/vue-plyr', ssr: false }
  ],

  components: true,
  buildModules: [
    '@nuxtjs/eslint-module',
  ],

  modules: [
  ],

  build: {
    publicPath: 'player',
    extractCSS: true,
    vendor: [ 'vue-plyr' ]
  },

  publicRuntimeConfig: {
    publicApiRoot: process.env.PUBLIC_API_ROOT
  },
  privateRuntimeConfig: {
    privateApiRoot: process.env.PRIVATE_API_ROOT
  }
}
