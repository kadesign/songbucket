import Vue from 'vue';
import VuePlyr from 'vue-plyr/dist/vue-plyr.ssr.js';
import 'vue-plyr/dist/vue-plyr.css';

Vue.use(VuePlyr, {
  plyr: {
    controls: ['play', 'current-time', 'progress', 'duration', 'mute', 'volume']
  }
});
