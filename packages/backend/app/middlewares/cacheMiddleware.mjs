export default class CacheMiddleware {
  static getMetaBySlug(cacheManager, logger) {
    return function (req, res, next) {
      const {slug} = req.params;

      cacheManager.get('nuxt:meta', slug)
        .then(data => {
          if (data) {
            logger.log(`Meta loaded from cache - key: "nuxt:meta:${slug}"`);
            res.send(data);
          } else {
            next();
          }
        })
        .catch(e => {
          logger.error(e);
          next();
        });
    }
  }
}