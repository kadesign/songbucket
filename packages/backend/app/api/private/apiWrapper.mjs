import express from 'express';

import CoverArtProcessor from '../../components/coverArtProcessor.mjs';
import CacheMiddleware from '../../middlewares/cacheMiddleware.mjs';
import MicroLog from '@kadesign/microlog';

export default class ApiWrapper {
  constructor(database, cacheManager) {
    this.logger = new MicroLog('PrivateApiWrapper');
    this.router = express.Router();
    this.database = database;
    this.cacheManager = cacheManager;
  }

  init() {
    this.router.get('/meta/:slug', CacheMiddleware.getMetaBySlug(this.cacheManager, this.logger), async (req, res) => {
      try {
        const trackMeta = await this.database.meta.getBySlug(req.params.slug);

        if (trackMeta) {
          this.logger.log(`Requested track meta - ID: ${trackMeta.id}, title: "${trackMeta.title}"`);

          let format;
          if (trackMeta.audioRef) {
            const audioData = await this.database.audio.getById(trackMeta.audioRef);
            if (audioData) {
              format = audioData.filename.substr(audioData.filename.lastIndexOf('.') + 1);
            } else {
              console.error(`Audio file with ID ${trackMeta.audioRef} not found but should be present in the database`);
              res.status(500).json({ error: 'Track audio not found but should be present' });
            }
          } else {
            res.status(404).json({ error: 'Track audio not uploaded' });
          }

          if (!trackMeta.coverArtColor) {
            trackMeta.coverArtColor = await CoverArtProcessor.extractAverageColor(trackMeta.coverArtFilename);
            await this.database.meta.updateCoverArtColor(trackMeta.id, trackMeta.coverArtColor);
          }

          const response = {
            artist: trackMeta.artist,
            title: trackMeta.title,
            coverArtFilename: trackMeta.coverArtFilename,
            coverColor: trackMeta.coverArtColor,
            isFinal: trackMeta.isFinal,
            isLoop: trackMeta.isLoop,
            version: trackMeta.version,
            format,
            createdAt: trackMeta.createdAt
          };

          this.cacheManager.set('nuxt:meta', req.params.slug, response, 3600);

          res.json(response);
        } else {
          this.logger.error(`Track with slug ${req.params.slug} not found`);
          res.status(404).json({error: 'Track not found'});
        }
      } catch (e) {
        this.logger.error(e);
        res.status(500).end();
      }
    });

    return this;
  }
}