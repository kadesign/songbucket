import express from 'express';
import path from 'path';

import AudioStreamer from '../../../components/audioStreamer.mjs';
import CoverArtProcessor from '../../../components/coverArtProcessor.mjs';

export default class ApiWrapper {
  constructor(database, cacheManager) {
    this.router = express.Router();
    this.database = database;
    this.cacheManager = cacheManager;
    this.audioStreamer = new AudioStreamer(this.database).init();
  }

  init() {
    this.router.use('/audio', this.audioStreamer.router);
    this.router.use('/cover-art', express.static(path.resolve(process.env.STORAGE_ROOT, 'cover')));
    this.router.get('/prepare-cover-art/:guid', (req, res) => {
      CoverArtProcessor.resize(req.params.guid).then(() => {
        res.status(200).end();
      })
    });
    this.router.post('/onerror/:slug', (req, res) => {
      this.cacheManager.del('nuxt:meta', req.params.slug).then(() => {
        res.status(200).end();
      }).catch(error => {
        res.status(500).json({ error });
      });
    });

    return this;
  }
}