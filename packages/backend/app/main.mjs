import express from 'express';

import Database from './components/db/database.mjs';
import CacheManager from './components/cacheManager.mjs';
import PrivateApiWrapper from './api/private/apiWrapper.mjs';
import PublicApiWrapperV1 from './api/public/v1/apiWrapper.mjs';

const app = express();
const database = new Database();
const cacheManager = new CacheManager();
const privateApiWrapper = new PrivateApiWrapper(database, cacheManager).init();
const publicApiWrapperV1 = new PublicApiWrapperV1(database, cacheManager).init();

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', process.env.ORIGIN);
  next();
});
app.use('/api/private', privateApiWrapper.router);
app.use('/api/public/v1', publicApiWrapperV1.router);

const serverPort = process.env.SERVER_PORT || 3000;
app.listen(serverPort);