import path from 'path';
import getColors from 'get-image-colors';
import fs from 'fs';
import sharp from 'sharp';

export default class CoverArtProcessor {
  static async extractAverageColor(filename) {
    console.log(`Extracting average color from ${filename}`)
    let calculated = (await getColors(path.resolve(process.env.STORAGE_ROOT, 'cover', filename), { count: 1 }))[0];
    if (calculated.get('hsl.l') > 0.65) calculated = calculated.set('hsl.l', 0.65);
    return calculated.hex();
  }

  static async resize(guid) {
    const sizes = [80, 120, 250, 300, 500];
    const img = path.resolve(process.env.STORAGE_ROOT, 'cover', guid + '.jpg');

    await Promise.all(sizes.map(async size => {
      const resizedPath = path.resolve(process.env.STORAGE_ROOT, 'cover', `${guid}-t${size}x${size}.jpg`);
      if (!fs.existsSync(resizedPath)) {
        await sharp(img).resize({
          width: size,
          kernel: sharp.kernel.lanczos3,
          fastShrinkOnLoad: false
        }).jpeg({ quality: 100 })
          .toFile(resizedPath);
        console.log(`File ${resizedPath} created`);
      }
    }));
  }
}