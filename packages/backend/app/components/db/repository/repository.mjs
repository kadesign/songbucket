export default class Repository {
  constructor(client, table) {
    this._client = client;
    this._table = table;
  }

  get client() {
    return this._client.from(this._table);
  }
}
