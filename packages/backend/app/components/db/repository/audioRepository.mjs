import Repository from './repository.mjs';

export default class AudioRepository extends Repository {
  constructor(supabase) {
    super(supabase, 'audio');
  }

  async getById(id) {
    const {data: audioData, error} = await this.client.select('filename').eq('id', id);
    if (error) throw error;

    if (audioData && audioData.length > 0) return audioData[0];
    return null;
  }
}