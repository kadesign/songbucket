import Repository from './repository.mjs';

export default class MetaRepository extends Repository {
  constructor(supabase) {
    super(supabase, 'meta');
  }

  async getAllRows() {
    const { data: rows, error } = await this.client.select('*');
    if (error) throw error;
    return rows.map(row => {
      return {
        id: row.id,
        artist: row.artist,
        title: row.title,
        version: row.version,
        slug: row.slug,
        coverArtFilename: row.cover_art_filename,
        coverArtColor: row.cover_art_color,
        isFinal: row.is_final,
        isLoop: row.is_loop,
        createdAt: row.created_at,
        uploadedAt: row.uploaded_at,
        audioRef: row.audio_ref,
        dawProjectRef: row.daw_project_ref
      }
    });
  }

  async getBySlug(slug) {
    const { data: rows, error } = await this.client
      .select('id,artist,title,version,cover_art_filename,cover_art_color,is_final,is_loop,created_at,audio_ref')
      .eq('slug', slug);
    if (error) throw error;
    return rows && rows.length > 0 ? {
      id: rows[0].id,
      artist: rows[0].artist,
      title: rows[0].title,
      version: rows[0].version,
      coverArtFilename: rows[0].cover_art_filename,
      coverArtColor: rows[0].cover_art_color,
      isFinal: rows[0].is_final,
      isLoop: rows[0].is_loop,
      createdAt: rows[0].created_at,
      audioRef: rows[0].audio_ref
    } : null;
  }

  async updateCoverArtColor(id, color) {
    const { error } = await this.client.update({ cover_art_color: color }).eq('id', id);
    if (error) throw error;
  }
}