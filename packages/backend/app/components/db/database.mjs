import { createClient } from '@supabase/supabase-js';
import MetaRepository from './repository/metaRepository.mjs';
import AudioRepository from './repository/audioRepository.mjs';

export default class Database {
  constructor() {
    this._supabase = createClient(process.env.SUPABASE_URL, process.env.SUPABASE_KEY);
    this.meta = new MetaRepository(this._supabase);
    this.audio = new AudioRepository(this._supabase);
  }

  async execProcedure(name, params) {
    let { data, error } = await this._supabase.rpc(name, params);
    if (error) throw error;
    return data;
  }
}