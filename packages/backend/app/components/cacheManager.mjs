import redis from 'async-redis';
import MicroLog from '@kadesign/microlog';

export default class CacheManager {
  constructor() {
    this.logger = new MicroLog(this);
    this.ignoreCache = Boolean(process.env.IGNORE_CACHE);
    if (!this.ignoreCache) {
      const host = process.env.REDIS_HOST;
      const port = process.env.REDIS_PORT;
      this._redisClient = redis.createClient(port, host);
    } else {
      this.logger.warn('IGNORE_CACHE is set to true, all requests to cache will be ignored');
    }
  }

  async set(key, id, value, expiresIn = 0) {
    if (this.ignoreCache) return;

    const strValue = JSON.stringify(value);
    if (expiresIn) {
      await this._redisClient.setex(`${key}:${id}`, expiresIn, strValue);
      this.logger.log(`${key}:${id} << ${this._head(strValue, 150)}`);
    } else {
      await this._redisClient.set(`${key}:${id}`, strValue);
      this.logger.log(`${key}:${id} (TTL: ${expiresIn}s) << ${this._head(strValue, 150)}`);
    }
  }

  async get(key, id) {
    if (this.ignoreCache) return null;

    const value = await this._redisClient.get(`${key}:${id}`);
    if (value) {
      this.logger.log(`${key}:${id} >> ${this._head(value, 150)}`);
      return JSON.parse(value);
    }
    this.logger.log(`${key}:${id} >> null`);
    return null;
  }

  async del(key, id) {
    if (this.ignoreCache) return;

    await this._redisClient.del(`${key}:${id}`);
    this.logger.log(`${key}:${id} deleted`);
  }

  _head(str, length) {
    if (str.length > length) return str.substr(0, length) + '...';
    return str;
  }
}