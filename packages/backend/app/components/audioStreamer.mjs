import express from 'express';
import fs from 'fs';
import path from 'path';
import { promisify } from 'util';

import FileReadStreams from '../model/fileReadStreams.mjs';
import MicroLog from '@kadesign/microlog';

const fstat = promisify(fs.stat);

export default class AudioStreamer {
  constructor(database) {
    this.logger = new MicroLog(this).setOutputDateInUTC();
    this.router = express.Router();
    this.database = database;
    this.fileReadStreams = new FileReadStreams();
  }

  init() {
    this.router.get('/:slug', async (req, res) => {
      try {
        const audioInfo = await this.database.execProcedure('get_audio', { query_slug: req.params.slug });

        if (audioInfo && audioInfo.length > 0) {
          this.logger.log(`Requested ${audioInfo[0].filename}, ${req.headers.range || 'initial request'}`);
          const audio = path.resolve(process.env.STORAGE_ROOT, 'audio', audioInfo[0].local_path, audioInfo[0].filename);

          await this.streamFileChunked(audio, req, res);
        } else {
          this.logger.warn(`Audio with slug ${req.params.slug} not found`);
          res.status(404).end();
        }
      } catch (error) {
        this.logger.error(error);
        res.status(500).end();
      }
    });

    return this;
  }

  async streamFileChunked(file, req, res) {
    try {
      const fileStat = await fstat(file);

      let chunkSize = 1024 * 1024;
      if(fileStat.size > chunkSize * 2) {
        chunkSize = Math.ceil(fileStat.size * 0.25);
      }
      let range = (req.headers.range) ? req.headers.range.replace(/bytes=/, "").split("-") : [];

      range[0] = range[0] ? parseInt(range[0], 10) : 0;
      range[1] = range[1] ? parseInt(range[1], 10) : range[0] + chunkSize;
      if(range[1] > fileStat.size - 1) {
        range[1] = fileStat.size - 1;
      }
      range = { start: range[0], end: range[1] };

      let stream = this.fileReadStreams.make(file, range);
      res.writeHead(206, {
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        'Pragma': 'no-cache',
        'Expires': 0,
        'Content-Type': 'audio/mpeg',
        'Accept-Ranges': 'bytes',
        'Content-Range': 'bytes ' + range.start + '-' + range.end + '/' + fileStat.size,
        'Content-Length': range.end - range.start + 1,
      });
      stream.pipe(res);
    } catch (e) {
      this.logger.error(e);
      return res.status(404).end();
    }
  }
}